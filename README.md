# SharkBaitOS

## Getting Started

Initialise the local repository:
```
repo init -u git://github.com/wantguns/manifest.git -b ten
```

Then sync:
```
repo sync -j$(nproc --all)
```

## Read More

[shark-bait](www.shark-bait.org)